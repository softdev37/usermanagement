
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author User
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    
    // Mockup
    static {
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
    }
    //create
    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
    public static boolean addUser(String userName, String password){
        userList.add(new User(userName, password));
        return true;
    }
    //update
    public static boolean updateUser(int index,User user){
        userList.set(index, user);
        return true;
    }
    //push data (edit) for update
    //read 1 user
    public static User getUser(int index){
        if(index > userList.size()-1){
            return null;
        }
        return userList.get(index);
    }
    //read all  user list
    public static ArrayList<User> getUsers(){
        return userList;
    }
    //search userName
    public static ArrayList<User> searchUserName(String searchText){
        ArrayList<User> list = new ArrayList<>();
        for(User user : userList) {
            if(user.getUserName().startsWith(searchText)){
                list.add(user);
            }
        }
        return list;
    }
    //delete user
    public static boolean delUser(int index){
        userList.remove(index);
        return true;
    }
    //delete user
    public static boolean delUser(User user){
        userList.remove(user);
        return true;
    }
    //Login
    public static User login(String userName, String password){
        for(User user: userList){
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    } 
    public static void save(){
        
    }
    public static void load(){
        
    }
}
